from number import number


class contact:
    """docstring for contact"""

    def __init__(self, name, num):
        self.__name = name
        self.__num = num
        self.els = {}
        self.add_number(self.__num)

    def get_name(self):
        return self.__name

    def get_con(self):
        return self.__num

    def set_name(self, name):
        self.__name = name

    def add_number(self, num):
        if num.get_type() in self.els:
            self.els[num.get_type()].append(num.get_nr())
        else:
            self.els[num.get_type()] = [num.get_nr()]


if __name__ == '__main__':
    numare = number('mobil', '0754757469')
    num1 = number('mobil', '32458092')
    num2 = number('fix', '245827392')
    num3 = number('fix', '07346245264')
    nume = input('Name = ')
    cont = contact(nume, 'mobil', '076426365')
    print(cont.get_contact())

    cont.add_number(num1)

    print(cont.els)
#   cont.delete_number(numare)
    print(cont.els)
    cont.add_number(num2)
    print(cont.els)
#    cont.delete_number(num2)
    print(cont.els)
    cont.add_number(num3)
    print(cont.get_contact())
