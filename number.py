class number:
    def __init__(self, type, nr):
        self.__type = type
        self.__nr = nr

    def get_type(self):
        return self.__type

    def get_nr(self):
        return self.__nr

    def get_elements(self):
        return {self.__type: [self.__nr]}


if __name__ == '__main__':
    num = number('fix', '036736473')

    dic = num.get_nr()
    print(dic)
